<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    public function brand(){
        return $this->belongsTo(Brand::class,'brandId','brandId');
    }

    public function designation(){
        return $this->belongsTo(Designation::class,'designationId','designationId');
    }

    public function department(){
        return $this->belongsTo(Department::class,'departmentId','departmentId');
    }

    public function shift(){
        return $this->belongsTo(Shift::class,'shiftId','shiftId');
    }
}
