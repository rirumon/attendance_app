<?php

namespace App\Http\Controllers;

use App\Models\Shift;
use Illuminate\Http\Request;

class ShiftController extends Controller
{
    public function index()
    {
        return view('admin.shift.index')->with('shift',Shift::all());
    }

    public function create()
    {
        return view('admin.shift.create');
    }

    public function store(Request $request)
    {
        // return $request;
        $shift = new Shift();
        $shift->shiftName = $request->shiftName ;
        $shift->inTime = $request->inTime ;
        $shift->outTime = $request->outTime ;
        $shift->late = $request->late ;
        $shift->early = $request->early ;
        $shift->beginningIn = $request->beginningIn ;
        $shift->endingIn = $request->endingIn ;
        $shift->beginningOut = $request->beginningOut ;
        $shift->endingOut = $request->endingOut ;
        $shift->Saturday = $request->Saturday == null ? 0 : 1 ;
        $shift->Sunday = $request->Sunday == null ? 0 : 1;
        $shift->Monday = $request->Monday == null ? 0 : 1;
        $shift->Tuesday = $request->Tuesday == null ? 0 : 1;
        $shift->Wednesday = $request->Wednesday == null ? 0 : 1;
        $shift->Thursday = $request->Thursday == null ? 0 : 1;
        $shift->Friday = $request->Friday == null ? 0 : 1;
        if($shift->save()){
            return redirect()->back()->with('success','Shift Create Successfully');
        }else{
            return redirect()->back()->with('failed','There are Some Problem Try again');
        }
    }


    public function edit($id)
    {
        $shift = Shift::where('shiftId',$id)->first();
        return view('admin.shift.edit')->with('shift',$shift);
    }

    public function update(Request $request)
    {
        $shift = Shift::where('shiftId',$request->shiftId)->update([
        'shiftName' => $request->shiftName,
        'inTime' => $request->inTime,
        'outTime' => $request->outTime,
        'late' => $request->late,
        'early' => $request->early,
        'beginningIn' => $request->beginningIn,
        'endingIn' => $request->endingIn,
        'beginningOut' => $request->beginningOut,
        'endingOut' => $request->endingOut,
        'Saturday' => $request->Saturday == null ? 0 : 1,
        'Sunday' => $request->Sunday == null ? 0 : 1,
        'Monday' => $request->Monday == null ? 0 : 1,
        'Tuesday' => $request->Tuesday == null ? 0 : 1,
        'Wednesday' => $request->Wednesday == null ? 0 : 1,
        'Thursday' => $request->Thursday == null ? 0 : 1,
        'Friday' => $request->Friday == null ? 0 : 1
        ]);
        if($shift){
            return redirect()->back()->with('success','Shift Update Successfully');
        }else{
            return redirect()->back()->with('failed','There are Some Problem Try again');
        }
    }


    public function destroy($id)
    {
        $shift = Shift::where('shiftId',$id);
        if($shift->delete()){
            return redirect()->back()->with('success', 'Shift Deleted Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }
}
