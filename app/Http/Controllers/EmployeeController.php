<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Department;
use App\Models\Designation;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employee=Employee::with('department')
                 ->with('shift')
            ->with('designation')
            ->orderByDesc('employeeId')->where('termination',0)->get();

        return view('admin.employee.index')->with('employee',$employee);
    }

    public function create()
    {
        $designation = Designation::all();
        $department =Department::all();
        $shift = Shift::all();
        return view('admin.employee.create')
            ->with('designation',$designation)
            ->with('shift',$shift)
            ->with('department',$department);
    }

    public function store(Request $request)
    {
        try{
            $request->validate([
                'fullName'=>'required',
                'phone'=>'required',
//                'photo'=>'required',
                'salary'=>'required',
                'religion'=>'required',
                'marital'=>'required',
                'departmentId'=>'required',
                'designationId'=>'required',
                'shiftId'=>'required',
                'joinDate'=>'required',
            ]);

            $employee = new Employee();
            $employee->fullName =$request->fullName;
            $employee->phone =$request->phone;
            $employee->nid =$request->nid ;
            $employee->salary =$request->salary ;
            $employee->religion =$request->religion ;
            $employee->marital =$request->marital ;
            $employee->departmentId =$request->departmentId;
            $employee->designationId =$request->designationId;
            $employee->shiftId =$request->shiftId;
            $employee->joinDate =$request->joinDate;
            $employee->address = $request->address;
            $employee->note =$request->note;
            $employee->emgContact =$request->emgContact;
            $employee->email =$request->email;
            $employee->whatsApp =$request->whatsApp;
            //there are the upload file

            //photos upload
            $unic =preg_replace('/\s+/','',$request->fullName);
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $photo = 'photo'.$unic . time() . '.' . $extension;
                $file->move('uploads/employee/', $photo);
                $employee->photo = $photo;
            }
            //cv upload
            if($request->hasFile('cv')){
                $file =$request->file('cv');
                $extension1 =$file->getClientOriginalExtension();
                $cv = 'cv'.$unic.time().'.'.$extension1;
                $file->move('uploads/employee/',$cv);
                $employee->cv = $cv;
            }
            //document1
            if($request->hasFile('document1')){
                $file =$request->file('document1');
                $extension2 =$file->getClientOriginalExtension();
                $document1 = 'document1'.$unic.time().'.'.$extension2;
                $file->move('uploads/employee/',$document1);
                $employee->document1 = $document1;
            }
            //document2
            if($request->hasFile('document2')){
                $file =$request->file('document2');
                $extension3 =$file->getClientOriginalExtension();
                $document2 = 'document2'.$unic.time().'.'.$extension3;
                $file->move('uploads/employee/',$document2);
                $employee->document2 = $document2;
            }
//            $employee->photo = '$photo';
            if($employee->save()){
                return redirect()->back()->with('success','Employee Create Successfully');
            }else{
                return redirect()->back()->with('failed','There are Some Problem Try again');
            }
        }catch (Exception $e){
            return redirect()->back()->with('failed','There are Some Problem Try again '.$e);
        }


    }

    public function show($id)
    {
        $employee=Employee::with('department')
            ->with('designation')
            ->with('shift')
            ->where('employeeId',$id)->where('termination',FALSE)->first();
        return view('admin.employee.show')->with('employee',$employee);
    }

    public function edit($id)
    {
        $employee=Employee::with('department')
            ->with('designation')
            ->with('shift')
            ->where('employeeId',$id)->where('termination',FALSE)->first();
        $designation = Designation::all();
        $department =Department::all();
        $shift =Shift::all();
        return view('admin.employee.edit')
            ->with('employee',$employee)
            ->with('designation',$designation)
            ->with('shift',$shift)
            ->with('department',$department);
    }

    public function update(Request $request)
    {

        try{
            $request->validate([
                'fullName'=>'required',
                'phone'=>'required',
                'photo'=>'required',
                'salary'=>'required',
                'religion'=>'required',
                'marital'=>'required',
                'departmentId'=>'required',
                'designationId'=>'required',
                'shiftId'=>'required',
                'joinDate'=>'required',
            ]);
            $employee = new Employee();
            $unic =preg_replace('/\s+/','',$request->fullName);
            //this cv
            if($request->hasFile('cvNew')){
                $file =$request->file('cvNew');
                $extension =$file->getClientOriginalExtension();
                $cv2New = 'cvedit'.$unic.time().'.'.$extension;
                $file->move('uploads/employee/',$cv2New);
                $employee->cv = $cv2New;
                //delete old data form server
                if($request->cv !=null){
                    $image_path = "uploads/employee/".$request->cv;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            }else{
                $employee->cv = $request->cv;
            }
            //document1
            if($request->hasFile('document1New')){
                $file = $request->file('document1New');
                $extension1 =$file->getClientOriginalExtension();
                $document1New = 'document1edit'.$unic.time().'.'.$extension1;
                $file->move('uploads/employee/',$document1New);
                $employee->document1= $document1New;

                //delete old data form server
                if($request->document1 != null){
                    $image_path = "uploads/employee/".$request->document1;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            }else{
                $employee->document1 = $request->document1;
            }
            //document2
            if($request->hasFile('document2New')){
                $file = $request->file('document2New');
                $extension2 =$file->getClientOriginalExtension();
                $document2New = 'document2edit'.$unic.time().'.'.$extension2;
                $file->move('uploads/employee/',$document2New);
                $employee->document2= $document2New;

                //delete old data form server
                if($request->document2 != null){
                    $image_path = "uploads/employee/".$request->document2;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            }else{
                $employee->document2 = $request->document2;
            }
            //photo
            if($request->hasFile('photoNew')){
                $file = $request->file('photoNew');
                $extension3 =$file->getClientOriginalExtension();
                $photoNew = 'photoedit'.$unic.time().'.'.$extension3;
                $file->move('uploads/employee/',$photoNew);
                $employee->photo= $photoNew;

                //delete old data form server
                if($request->photo != null){
                    $image_path = "uploads/employee/".$request->photo;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
            }else{
                $employee->photo = $request->photo;
            }
            //update
            $empUpdate = Employee::Where('employeeId',$request->employeeId)->update([
                'fullName' => $request->fullName,
                'phone' => $request->phone,
                'nid' => $request->nid,
                'salary' => $request->salary,
                'religion' => $request->religion,
                'marital' => $request->marital,
                'address' => $request->address,
                'note' => $request->note,
                'emgContact' => $request->emgContact,
                'departmentId' => $request->departmentId,
                'designationId' => $request->designationId,
                'shiftId' => $request->shiftId,
                'joinDate' => $request->joinDate,
                'email' => $request->email,
                'whatsApp' => $request->whatsApp,
                'photo' => $employee->photo,
                'cv' => $employee->cv,
                'document1' => $employee->document1,
                'document2' => $employee->document2,
            ]);

            if($empUpdate){
                return redirect()->back()->with('success','Employee Update Successfully');
            }else{
                return redirect()->back()->with('failed','There are Some Problem Try again');
            }
        }catch (Exception $e){
            return redirect()->back()->with('failed','There are Some Problem Try again '.$e);
        }

    }

    public function destroy($id)
    {
        try{
            $employee = Employee::where('employeeId',$id)->first();
            if($employee->termination == 1 || $employee->uses == 1){
                return redirect()->back()->with('success', 'You Cannot Delete It, It Have related data First delete Those !');
            }else{
                //delete hard file
                if($employee->photo !=null){
                    $image_path = "uploads/employee/".$employee->photo;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
                if($employee->cv !=null){
                    $image_path = "uploads/employee/".$employee->cv;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
                if($employee->document1 !=null){
                    $image_path = "uploads/employee/".$employee->document1;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
                if($employee->document2 !=null){
                    $image_path = "uploads/employee/".$employee->document2;
                    if (file_exists($image_path)) {
                        unlink($image_path);
                    }
                }
                if(Employee::where('employeeId',$employee->employeeId)->delete()){
                    return redirect()->back()->with('success', 'Employee Deleted Successfully');
                } else {
                    return redirect()->back()->with('failed', 'There are Some Problem Try again');
                }
            }
        }catch (Exception $e){
            return redirect()->back()->with('failed','There are Some Problem Try again '.$e);
        }


    }
}
