<?php

namespace App\Http\Controllers;

use App\Models\WeekHoliday;
use Illuminate\Http\Request;

class WeekHolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WeekHoliday  $weekHoliday
     * @return \Illuminate\Http\Response
     */
    public function show(WeekHoliday $weekHoliday)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WeekHoliday  $weekHoliday
     * @return \Illuminate\Http\Response
     */
    public function edit(WeekHoliday $weekHoliday)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WeekHoliday  $weekHoliday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeekHoliday $weekHoliday)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WeekHoliday  $weekHoliday
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeekHoliday $weekHoliday)
    {
        //
    }
}
