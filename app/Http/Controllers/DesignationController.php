<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function index()
    {
        $designation = Designation::all();
        return view('admin.designation.index')->with('designation',$designation);
    }

    public function store(Request $request)
    {
        $request->validate([
          'designation'=>'required'
        ]);
        $designation = new Designation();
        $designation->designation = $request->designation;
        if($designation->save()){
            return redirect()->back()->with('success', 'Designation Create Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }

    public function edit($id)
    {
        $designation = Designation::where('designationId',$id)->first();
        if ($designation == null) {
            return view('admin.designation.edit')->with('notFound', 'Data is not found');
        } else {
            return view('admin.designation.edit')->with('designation',$designation);
        }
    }

    public function update(Request $request)
    {
        $designation = Designation::where('designationId',$request->designationId)->update([
            'designation'=>$request->designation
        ]);

        if ($designation) {
            return redirect('/admin/designation/index')->with('success', 'Designation Update Successfully');
        } else {
            return redirect('/admin/designation/index')->with('failed', 'There are Some Problem Try again');
        }
    }


    public function destroy($id)
    {
        $designation = Designation::where('designationId',$id);
        if($designation->delete()){
            return redirect()->back()->with('success', 'Designation Deleted Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }
}
