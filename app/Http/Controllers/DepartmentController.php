<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $department = Department::all();
        return view('admin.department.index')->with('department',$department);
    }

    public function store(Request $request)
    {
        $request->validate([
           'department'=>'required'
        ]);
        $department = new Department();
        $department->department = $request->department;
        if($department->save()){
            return redirect()->back()->with('success', 'Department Create Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }

    public function edit($id)
    {
        $department = Department::where('departmentId',$id)->first();
        if ($department == null) {
            return view('admin.department.edit')->with('notFound', 'Data is not found');
        } else {
            return view('admin.department.edit')->with('department',$department);
        }
    }


    public function update(Request $request)
    {
        $department = Department::where('departmentId',$request->departmentId)->update([
           'department'=>$request->department
        ]);
        if ($department) {
            return redirect('/admin/department/index')->with('success', 'Department Update Successfully');
        } else {
            return redirect('/admin/department/index')->with('failed', 'There are Some Problem Try again');
        }
    }

    public function destroy($id)
    {
        $department = Department::where('departmentId',$id);
        if($department->delete()){
            return redirect()->back()->with('success', 'Department Deleted Successfully');
        } else {
            return redirect()->back()->with('failed', 'There are Some Problem Try again');
        }
    }
}
