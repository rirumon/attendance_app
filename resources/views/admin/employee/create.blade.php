@extends('master')
@section('content')

    <div class="row">

                <div class="card">
                    <div class="card-header">
                        Employee Create
                    </div>

                    <div class="card-body">
                        <form action="{{ url("admin/employee/store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="">
                                <div class="form-group">
                                    <label>Employee Full Name</label>
                                    <input class="form-control" name="fullName" type="text" required>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label>Employee Photo</label>
                                        <input type="file" name="photo" class="form-control-file">
                                    </div>
                                    <div class="col-6">
              <!--Images preview-->
                                        <img src="https://placehold.it/80x80" id="preview" class="img-thumbnail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Employee Phone Number</label>
                                    <input class="form-control" name="phone" type="text" required>
                                </div>
                                <div class="form-group">
                                    <label>Employee Email Address</label>
                                    <input class="form-control" name="email" type="email" required>
                                </div>
                                <div class="form-group">
                                    <label>Employee WhatsApp Number</label>
                                    <input class="form-control" name="whatsApp" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Employee NID Number</label>
                                    <input class="form-control" name="nid" type="text" >
                                </div>
                                <div class="form-group">
                                    <label>Employee Salary</label>
                                    <input class="form-control" name="salary" type="number" required>
                                </div>
                                <div class="form-group">
                                    <label>Religion</label>
                                    <select class="form-control select2" name="religion" >
                                            <option value="Islam">Islam</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Marital Status</label>
                                    <select class="form-control select2" name="marital" >
                                        <option value="UnMarried">UnMarried</option>
                                        <option value="Married">Married</option>
                                    </select>
                                </div>
<hr>
                                <div class="row">
                                    
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Department</label>
                                            <select class="form-control select2" name="departmentId" required>
                                                @foreach($department as $department)
                                                    <option value="{{$department->departmentId}}">{{$department->department}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Designation</label>
                                            <select class="form-control select2" name="designationId" required>
                                                @foreach($designation as $designation)
                                                    <option value="{{$designation->designationId}}">{{$designation->designation}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
<hr>
                                <div class="form-group">
                                    <label>Join Date</label>
                                    <input class="form-control date" name="joinDate" type="date" required>
                                </div>

                               <hr/>
                                <div class="form-group">
                                    <label>Emergency Contact</label>
                                    <textarea class="textarea" name="emgContact" rows="5" ></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="textarea" name="address" rows="5" ></textarea>
                                </div>
                         <div class="form-group">
                                    <label>Note</label>
                                    <textarea class="textarea" name="note" rows="5" ></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <label>CV</label>
                                        <input type="file" name="cv" class="form-control-file" >
                                    </div>
                                    <div class="col-4">
                                        <label>Document 1</label>
                                        <input type="file" name="document1" class="form-control-file" >
                                    </div>

                                    <div class="col-4">
                                        <label>Document 2</label>
                                        <input type="file" name="document2" class="form-control-file" >
                                    </div>
                                </div>
                            </div>

                            <div class="m-5">
                                <button class="btn btn-primary" type="submit"> Create</button>
                            </div>
                        </form>


                    </div>
                </div>

    </div>

@endsection


