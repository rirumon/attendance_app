@extends('master')
@section('content')
   
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ url("admin/employee/create") }}">
                   Add Employee
                </a>
            </div>
        </div>

    <div class="row">
     
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Employee List
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                            <thead>
                            <tr>
                                <th>
                                    Id
                                </th>
                                <th>
                                    Photo
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Department
                                </th>
                                <th>
                                    Contact
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $a=1?>
                            @foreach($employee as  $employee)
                                <tr data-entry-id="{{ $employee->employeeId }}">
                                    <td>
                                        {{ $a++ }}
                                    </td>
                                    <td>
                                        <img src="{{url('uploads/employee/'.$employee->photo)}}" class="img-round" width="80" height="80">
                                    </td>
                                    <td>
                                      <p><b>{{$employee->fullName}}</b></p>
                                    
                                    </td>
                                    <td>
                                        <p><b>{{$employee->department->department}}</b></p>
                                        <p><span class="text-muted">Designation: {{$employee->designation->designation}}</span></p>
                                    </td>
                                    <td>
                                        <p><b>{{$employee->phone}}</b></p>
                                        <p><span class="text-muted">Email: {{$employee->email}}</span></p>
                                        <p><span class="text-muted">WhatsApp: {{$employee->whatsApp}}</span></p>
                                    </td>
                                    <td>
                                  
                                            <a class="btn btn-xs btn-info" href="{{ url('admin/employee/edit', $employee->employeeId) }}">
                                                Edit
                                            </a>
                               
                                            <a class="btn btn-xs btn-info" href="{{ url('admin/employee/show', $employee->employeeId) }}">
                                                Show
                                            </a>
                   

                                     
                                            <form action="{{ url('admin/employee/destroy', $employee->employeeId) }}" method="POST" onsubmit="return confirm('deleted to confirm');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="btn btn-xs btn-danger" value="deleted">
                                            </form>
           
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection


