@extends('master')
@section('content')

    <div class="row">

            <div class="card">
                <div class="card-header">
                    Employee Update
                </div>

                <div class="card-body">
                    <form action="{{ url("admin/employee/update") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="employeeId" value="{{$employee->employeeId}}">
                        <div class="">
                            <div class="form-group">
                                <label>Employee Full Name</label>
                                <input class="form-control" name="fullName" type="text" value="{{$employee->fullName}}" required>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label>Employee Photo</label>
                                    <input type="file" name="photoNew" class="form-control-file">
                                    <input type="hidden" name="photo" value="{{$employee->photo}}">
                                </div>
                                <div class="col-6">
                                    <img src="{{url('uploads/employee/'.$employee->photo)}}" id="preview" class="img-thumbnail">

                                </div>
                            </div>
                            <div class="form-group">
                                <label>Employee Phone Number</label>
                                <input class="form-control" name="phone" type="text" value="{{$employee->phone}}" required>
                            </div>
                            <div class="form-group">
                                <label>Employee Email Address</label>
                                <input class="form-control" name="email" type="email" value="{{$employee->email}}" required>
                            </div>
                            <div class="form-group">
                                <label>Employee WhatsApp Number</label>
                                <input class="form-control" name="whatsApp" type="text" value="{{$employee->whatsApp}}">
                            </div>
                            <div class="form-group">
                                <label>Employee NID Number</label>
                                <input class="form-control" name="nid" type="text" value="{{$employee->nid}}">
                            </div>
                            <div class="form-group">
                                <label>Employee Salary</label>
                                <input class="form-control" name="salary" type="number"value="{{$employee->salary}}" required>
                            </div>
                            <div class="form-group">
                                <label>Religion</label>
                                <select class="form-control select2" name="religion" >
                                    <option value="Islam" {{'Islam' == $employee->religion ? 'selected' : null}}>Islam</option>
                                    <option value="Hindu" {{'Hindu' == $employee->religion ? 'selected' : null}}>Hindu</option>
                                    <option value="Other" {{'Other' == $employee->religion ? 'selected' : null}}>Other</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Marital Status</label>
                                <select class="form-control select2" name="marital" >
                                    <option value="UnMarried" {{'UnMarried' == $employee->marital ? 'selected' : null}}>UnMarried</option>
                                    <option value="Married" {{'Married' == $employee->marital ? 'selected' : null}}>Married</option>
                                </select>
                            </div>
                            <hr>
                            <div class="row">
                                
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select class="form-control select2" name="departmentId" required>
                                            @foreach($department as $department)
                                                <option value="{{$department->departmentId}}" {{$department->departmentId == $employee->department->departmentId ? 'selected' : null}}>{{$department->department}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Designation</label>
                                        <select class="form-control select2" name="designationId" required>
                                            @foreach($designation as $designation)
                                                <option value="{{$designation->designationId}}"  {{$designation->designationId == $employee->designation->designationId ? 'selected' : null}}>{{$designation->designation}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Join Date</label>
                                <input class="form-control date" name="joinDate"  readonly value="{{$employee->joinDate}}" required>
                            </div>

                            <hr/>
                            <div class="form-group">
                                <label>Emergency Contact</label>
                                <textarea class="textarea" name="emgContact" rows="5" >{{$employee->emgContact}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea class="textarea" name="address" rows="5" >{{$employee->address}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <textarea class="textarea" name="note" rows="5" >{{$employee->note}}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <label>CV</label>
                                    <input type="file" name="cvNew" class="form-control-file" >
                                    <input type="hidden" value="{{$employee->cv}}" name="cv">
                                    <a href="{{url('uploads/employee/'.$employee->cv)}}" target="_blank" class="nav-link">{{$employee->cv}}</a>
                                </div>
                                <div class="col-4">
                                    <label>Document 1</label>
                                    <input type="file" name="document1New" class="form-control-file" >
                                    <input type="hidden" value="{{$employee->document1}}" name="document1">
                                    <a href="{{url('uploads/employee/'.$employee->document1)}}" target="_blank" class="nav-link">{{$employee->document1}}</a>
                                </div>

                                <div class="col-4">
                                    <label>Document 2</label>
                                    <input type="file" name="document2New" class="form-control-file" >
                                    <input type="hidden" value="{{$employee->document2}}" name="document2">
                                    <a href="{{url('uploads/employee/'.$employee->document2)}}" target="_blank" class="nav-link">{{$employee->document2}}</a>
                                </div>
                            </div>
                        </div>

                        <div class="m-5">
                            <button class="btn btn-primary" type="submit"> Update</button>
                        </div>
                    </form>


                </div>
            </div>

    </div>

@endsection

