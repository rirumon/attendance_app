@extends('master')
@section('content')


  
            <div class="card">
                <div class="card-header bg-primary">
                    Employee Profile
                </div>

                <div class="card-body">
                  <div class="row">
                      <div class="col-6">
                      <div class="float-left">
                          <p><b>{{$employee->fullName}}</b></p>
                        
                          <p> Department: <span class="text-muted">{{$employee->department->department}}</span></p>
                          <p>Designation: <span class="text-muted">{{$employee->designation->designation}}</span></p>
                      </div>
                      </div>
                      <div class="col-6">
                          <div class="float-right">
                              <img src="{{url('uploads/employee/'.$employee->photo)}}" class="img-round" width="150" height="180">
                          </div>
                      </div>
                  </div>
                    <hr>
                    <div class="card m-3">
                        <div class="card-header">
                            Information
                        </div>
                        <div class="card-body">
                            <p> Phone: <span class="text-muted">{{$employee->phone}}</span></p>
                            <p> Email: <span class="text-muted">{{$employee->email}}</span></p>
                            <p> WhatsApp: <span class="text-muted">{{$employee->whatsApp}}</span></p>
                            <p> NID Number: <span class="text-muted">{{$employee->nid}}</span></p>
                            <p> Current Salary: <span class="text-muted">{{$employee->salary}}</span></p>
                            <p> Religion: <span class="text-muted">{{$employee->religion}}</span></p>
                            <p> Marital Status: <span class="text-muted">{{$employee->marital}}</span></p>
                            <p> Join Date: <span class="text-muted">{{$employee->joinDate}}</span></p>
                            <p> Address: <span class="text-muted">{{$employee->address}}</span></p>
                            <hr>
                            <p> Note: <span class="text-muted">{{$employee->note}}</span></p>
                            <p> Important Contact: <span class="text-muted">{{$employee->emgContact}}</span></p>
                        </div>
                    </div>
                    <hr>
                    <div class="card m-3">
                        <div class="card-header">
                            Document Link
                        </div>
                        <div class="card-body">
                                @if($employee->cv != null)
                                <a href="{{url('uploads/employee/'.$employee->cv)}}" target="_blank" class="nav-link">{{$employee->cv}}</a>
                                @endif
                                @if($employee->document1 != null)
                                    <a href="{{url('uploads/employee/'.$employee->document1)}}" target="_blank" class="nav-link">{{$employee->document1}}</a>
                                @endif
                                @if($employee->document2 != null)
                                    <a href="{{url('uploads/employee/'.$employee->document2)}}" target="_blank" class="nav-link">{{$employee->document2}}</a>
                                @endif

                        </div>
                    </div>
                </div>
            </div>



@endsection

