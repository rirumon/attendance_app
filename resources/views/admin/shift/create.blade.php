@extends('master')
@section('content')

<div class="">

    <div class="card">
        <div class="card-header">
            Shift Create
        </div>

        <div class="card-body">
            <form action="{{ url("admin/shift/store") }}" method="POST" enctype="multipart/form-data">
                @csrf

                
               <div class="form-group">
                   <label>Shift Name</label>
                   <input type="text" name="shiftName" class="form-control date">
               </div>
               <div class="form-group">
                <label>In Time</label>
                <input type="time" name="inTime" class="form-control">
               </div>
               <div class="form-group">
                <label>Out Time</label>
                <input type="time" name="outTime" class="form-control">
               </div>
               <div class="form-group">
                <label>Late Time (Minutes)</label>
                <input type="time" name="late" class="form-control">
               </div>
               <div class="form-group">
                <label>Early Time (Minutes)</label>
                <input type="time" name="early" class="form-control">
               </div>
               <div class="form-group">
                <label>BeginningIn Time</label>
                <input type="time" name="beginningIn" class="form-control">
               </div>
               <div class="form-group">
                <label>EndingIn Time</label>
                <input type="time" name="endingIn" class="form-control">
               </div>
               <div class="form-group">
                <label>BeginningOut Time</label>
                <input type="time" name="beginningOut" class="form-control">
               </div>
               <div class="form-group">
                <label>EndingOut Time</label>
                <input type="time" name="endingOut" class="form-control">
               </div>
               <div class="form-group">
                <label>EndingIn Time</label>
                <input type="time" name="endingIn" class="form-control">
               </div>
               <hr/>
               <div class="form-group">
                   <label>Check Vacition Day </label>
                   <div class="radio">
                    <label><input type="radio" name="Saturday">Saturday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Sunday">Sunday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Monday">Monday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Tuesday">Tuesday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Wednesday">Wednesday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Thursday">Thursday</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="Friday">Friday</label>
                  </div>
            

                <div class="m-5">
                    <button class="btn btn-primary" type="submit"> Create</button>
                </div>
            </form>


        </div>
    </div>

</div>

@endsection