@extends('master')
@section('content')
   
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ url("admin/shift/create") }}">
                   Add Shift
                </a>
            </div>
        </div>

    <div class="row">
     
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Shift List
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                            <thead>
                            <tr>
                                <th>
                                    Id
                                </th>
                                <th>
                                    Shift Name
                                </th>
                                <th>
                                    In Time
                                </th>
                                <th>
                                    Out Time
                                </th>
                                <th>
                                    Holiday
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $a=1?>
                            @foreach($shift as  $shift)
                                <tr data-entry-id="{{ $shift->shiftId }}">
                                    <td>
                                        {{ $a++ }}
                                    </td>
                                    <td>
                                         {{$shift->shiftName}}
                                    </td>
                                    <td>
                                      <p>In Time :<b>{{$shift->inTime}}</b></p>
                                      <p>Late In Time :<b>{{$shift->late}}</b></p>
                            
                                    
                                    </td>
                                    <td>
                                        <p>Out Time :<b>{{$shift->outTime}}</b></p>
                                      <p>Early Out Time :<b>{{$shift->early}}</b></p>
                                    </td>
                                    <td>
                                        @if($shift->Saturday)
                                        <p class="text-danger">Saturday</p>
                                        @endif
                                        @if($shift->Sunday)
                                        <p class="text-danger">Sunday</p>
                                        @endif
                                        @if($shift->Monday)
                                        <p class="text-danger">Monday</p>
                                        @endif
                                        @if($shift->Tuesday)
                                        <p class="text-danger">Tuesday</p>
                                        @endif
                                        @if($shift->Wednesday)
                                        <p class="text-danger">Wednesday</p>
                                        @endif
                                        @if($shift->Thursday)
                                        <p class="text-danger">Thursday</p>
                                        @endif
                                        @if($shift->Friday)
                                        <p class="text-danger">Friday</p>
                                        @endif
                                      
                                    </td>
                                    <td>
                                  
                                            <a class="btn btn-xs btn-info" href="{{ url('admin/shift/edit', $shift->shiftId) }}">
                                                Edit
                                            </a>
                               
                        
                                            <form action="{{ url('admin/shift/destroy', $shift->shiftId) }}" method="POST" onsubmit="return confirm('deleted to confirm');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="btn btn-xs btn-danger" value="deleted">
                                            </form>
           
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection


