@extends('master')
@section('content')

    <div class="row">

            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        Department Create
                    </div>

                    <div class="card-body">
                        <form action="{{ url("admin/department/store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Department Name*</label>
                                <input type="text" id="title" name="department" class="form-control"  required>
                            </div>
                            <div>
                                <button class="btn btn-primary" type="submit"> Create</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
     
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    Department List
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                            <thead>
                            <tr>
                                <th>
                                    Id
                                </th>
                                <th>
                                    Department
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $a=1?>
                            @foreach($department as  $department)
                                <tr data-entry-id="{{ $department->departmentId }}">
                                    <td>
                                        {{ $a++ }}
                                    </td>
                                    <td>
                                        {{ $department->department ?? '' }}
                                    </td>
                                    <td>
                                
                                            <a class="btn btn-xs btn-info" href="{{ url('admin/department/edit', $department->departmentId) }}">
                                                Edit
                                            </a>
                                

                                
                                            <form action="{{ url('admin/department/destroy', $department->departmentId) }}" method="POST" onsubmit="return confirm('Confirm to delete this');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="btn btn-xs btn-danger" value="Deleted">
                                            </form>
                                  

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection


