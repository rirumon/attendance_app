@extends('master')
@section('content')

    <div class="card">
        <div class="card-header">
            Department Update
        </div>

        <div class="card-body">
            <form action="{{ url("admin/department/update") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="hidden" name="departmentId" value="{{$department->departmentId}}">
                    <label for="title">Department Name *</label>
                    <input type="text"  name="department" class="form-control" value="{{ $department->department }}" required>

                </div>
                <div>
                    <button type="submit" class="btn btn-danger">Update</button>
                </div>
            </form>


        </div>
    </div>
@endsection
