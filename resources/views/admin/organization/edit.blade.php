@extends('layouts.admin')
@section('content')


            <div class="card">
                <div class="card-header">
                    Organization Update
                </div>

                <div class="card-body">
                    <form action="{{ url("admin/organization/update") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="organizationId" value="1">
                        <div class="">
                            <div class="form-group">
                                <label>Organization Name</label>
                                <input class="form-control" name="name" type="text" value="{{$organization->name}}" required>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label>Organization Logo </label>
                                    <input type="file" name="newLogo" class="form-control-file">
                                    <input type="hidden" name="logo" value="{{$organization->logo}}">
                                </div>
                                <div class="col-6">
                                    <img src="{{url('uploads/organization/'.$organization->logo)}}" id="preview" class="img-thumbnail">

                                </div>
                            </div>
                            <div class="form-group">
                                <label>About Organization</label>
                                <textarea class="textarea" name="about" rows="5" >{{$organization->about}}</textarea>
                            </div>

                            <hr/>
                            <div class="form-group">
                                <label>HeaderStyleText</label>
                                <textarea class="textarea" name="headerStyleText" rows="5" >{{$organization->headerStyleText}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <textarea class="textarea" name="address" rows="5" >{{$organization->address}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>FooterStyleText</label>
                                <textarea class="textarea" name="footerStyleText" rows="5" >{{$organization->footerStyleText}}</textarea>
                            </div>
                        
                        </div>

                        <div class="m-5">
                            <button class="btn btn-primary" type="submit"> Update</button>
                        </div>
                    </form>


                </div>
            </div>

@endsection

