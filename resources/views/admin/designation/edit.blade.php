@extends('master')
@section('content')

    <div class="card">
        <div class="card-header">
            Designation Update
        </div>

        <div class="card-body">
            <form action="{{ url("admin/designation/update") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="hidden" name="designationId" value="{{$designation->designationId}}">
                    <label for="title">Designation Name *</label>
                    <input type="text"  name="designation" class="form-control" value="{{ $designation->designation }}" required>

                </div>
                <div>
                    <button type="submit" class="btn btn-danger">Update</button>
                </div>
            </form>


        </div>
    </div>
@endsection
