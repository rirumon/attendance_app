@extends('master')
@section('content')

    <div class="row">

            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        Designation Create
                    </div>

                    <div class="card-body">
                        <form action="{{ url("admin/designation/store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Designation Name*</label>
                                <input type="text" id="title" name="designation" class="form-control"  required>
                            </div>
                            <div>
                                <button class="btn btn-primary" type="submit"> Create</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
     
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    Designation List
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                            <thead>
                            <tr>
                                <th>
                                    Id
                                </th>
                                <th>
                                    Designation
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $a=1?>
                            @foreach($designation as  $designation)
                                <tr data-entry-id="{{ $designation->designationId }}">
                                    <td>
                                        {{ $a++ }}
                                    </td>
                                    <td>
                                        {{ $designation->designation ?? '' }}
                                    </td>
                                    <td>
                                      
                                            <a class="btn btn-xs btn-info" href="{{ url('admin/designation/edit', $designation->designationId) }}">
                                                Edit
                                            </a>
                                   

                                 
                                            <form action="{{ url('admin/designation/destroy', $designation->designationId) }}" method="POST" onsubmit="return confirm('Delete to Confirm');" style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="submit" class="btn btn-xs btn-danger" value="Deleted">
                                            </form>
                                

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

