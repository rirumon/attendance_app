<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Laravel with AdminLTE </title>

    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/ekko-lightbox/ekko-lightbox.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/daterangepicker/daterangepicker.css">
      <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <link rel="stylesheet" href="{{asset('backEnd')}}/plugins/summernote/summernote-bs4.css">
    <!-- <link rel="stylesheet" href="{{asset('backEnd/dropzone/dropzone.min.css')}}"> -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    <!-- Navbar -->
    @include('include.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('include.aside')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <section class="content">
            <div class="container-fluid">
        <!-- There are the all runtime error show-->
        @if (count($errors) > 0)
            <div class="alert alert-danger m-3">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show  m-3" role="alert" id="gone">
                <strong>{{$message}}.</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if($mess =Session::get('failed'))
            <div class="alert alert-warning alert-dismissible fade show  m-3" role="alert" id="gone">
                <strong>{{$mess}}.</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    @yield('content')
    </div><!--/. container-fluid -->
        </section>
    </div>
    <!-- /.content-wrapper -->

    @include('include.footer')
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="{{asset('backEnd')}}/plugins/jquery/jquery.js"></script>
<script src="{{asset('backEnd')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/moment/moment.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('backEnd')}}/plugins/select2/js/select2.full.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="{{asset('backEnd')}}/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="{{asset('backEnd')}}/plugins/raphael/raphael.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/chart.js/Chart.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- <script src="{{asset('backEnd')}}/dist/js/pages/dashboard2.js"></script> -->
<script src="{{asset('backEnd')}}/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{asset('backEnd')}}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script src="{{asset('backEnd')}}//plugins/filterizr/jquery.filterizr.min.js"></script>
<script src="{{asset('backEnd')}}/plugins/summernote/summernote-bs4.min.js"></script>
<!-- <script src="{{asset('backEnd/dropzone/dropzone.min.js')}}"></script> -->
<script src="{{asset('backEnd')}}/dist/js/adminlte.js"></script>
<script src="{{asset('backEnd')}}/dist/js/demo.js"></script>
<script src="{{asset('main.js')}}"></script>
</body>
</html>

