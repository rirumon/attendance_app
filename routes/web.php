<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::redirect('/admin','/admin/employee/index');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
     
    //designation
    Route::delete('designation/destroy/{id}','DesignationController@destroy');
    Route::get('designation/index','DesignationController@index');
    Route::post('designation/store','DesignationController@store');
    Route::get('designation/edit/{id}','DesignationController@edit');
    Route::post('designation/update','DesignationController@update');

    //department
    Route::delete('department/destroy/{id}','DepartmentController@destroy');
    Route::get('department/index','DepartmentController@index');
    Route::post('department/store','DepartmentController@store');
    Route::get('department/edit/{id}','DepartmentController@edit');
    Route::post('department/update','DepartmentController@update');

    //employee
    Route::delete('employee/destroy/{id}','EmployeeController@destroy');
    Route::get('employee/index','EmployeeController@index');
    Route::post('employee/store','EmployeeController@store');
    Route::get('employee/edit/{id}','EmployeeController@edit');
    Route::post('employee/update','EmployeeController@update');
    Route::get('employee/show/{id}','EmployeeController@show');
    Route::get('employee/create','EmployeeController@create');


    //Organization 
    Route::get('organization/edit','OrganizationController@edit');
    Route::post('organization/update','OrganizationController@update');
   
    //Shift
    Route::delete('shift/destroy/{id}','ShiftController@destroy');
    Route::get('shift/index','ShiftController@index');
    Route::get('shift/create','ShiftController@create');
    Route::post('shift/store','ShiftController@store');
    Route::get('shift/edit/{id}','ShiftController@edit');
    Route::post('shift/update/{id}','ShiftController@update');
 
    
});