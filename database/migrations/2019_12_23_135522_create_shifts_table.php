<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->bigIncrements('shiftId');
            $table->string('shiftName');
            $table->time('inTime');
            $table->time('outTime');
            $table->time('late')->nullable();
            $table->time('early')->nullable();
            $table->time('beginningIn')->nullable();
            $table->time('endingIn')->nullable();
            $table->time('beginningOut')->nullable();
            $table->time('endingOut')->nullable();
            //there are the vacation list
            $table->boolean('Saturday')->default(false);
            $table->boolean('Sunday')->default(false);
            $table->boolean('Monday')->default(false);
            $table->boolean('Tuesday')->default(false);
            $table->boolean('Wednesday')->default(false);
            $table->boolean('Thursday')->default(false);
            $table->boolean('Friday')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
