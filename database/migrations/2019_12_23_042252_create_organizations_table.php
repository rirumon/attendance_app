<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('organizationsId');
            $table->string('name');
            $table->string('about');
            $table->string('headerStyleText')->nullable();
            $table->string('footerStyleText')->nullable();
            $table->string('logo')->nullable();
            $table->string('address')->nullable();
            $table->string('unicId')->nullable();
            $table->string('unicPassword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
