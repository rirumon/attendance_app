<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('employeeId');
            $table->string('fullName');
            // $table->string('Id')
            $table->string('biometric')->nullable();
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('whatsApp')->nullable();
            $table->string('nid')->nullable();
            $table->string('photo');
            $table->integer('salary');
            $table->boolean('termination')->default(FALSE);
            $table->boolean('uses')->default(false);
            $table->enum('religion',['Islam','Hindu','Others']);
            $table->enum('marital',['UnMarried','Married']);
            $table->bigInteger('departmentId')->unsigned();
            $table->foreign('departmentId')->on('departments')->references('departmentId');
            $table->bigInteger('designationId')->unsigned();
            $table->foreign('designationId')->on('designations')->references('designationId');
            $table->bigInteger('shiftId')->unsigned();
            $table->foreign('shiftId')->references('shiftId')->on('shifts');
            $table->date('joinDate');
            $table->string('address');
            $table->string('note');
            $table->string('emgContact');
            //document
            $table->string('cv')->nullable();
            $table->string('document1')->nullable();
            $table->string('document2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
